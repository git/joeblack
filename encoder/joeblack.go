package main

import (
	"bufio"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"

	"git.sr.ht/~sircmpwn/go-bare"
	"github.com/codahale/sss"
	"github.com/google/shlex"
)

func main() {
	wordlistFile, err := os.Open("wordlist")
	if err != nil {
		fmt.Printf("while opening wordlist: %v\n", err)
		os.Exit(1)
	}
	defer wordlistFile.Close()

	wordlist := make([]string, 2048)
	wordlistRev := map[string]int{}
	for i := 0; i < 2048; i++ {
		fmt.Fscanf(wordlistFile, "%s", &wordlist[i])
		wordlistRev[wordlist[i]] = i
	}

	recreate := false
	add := false
	var already int64 = 0
	if len(os.Args) > 1 && os.Args[1] == "-r" {
		recreate = true
	}
	if len(os.Args) > 2 && os.Args[1] == "-a" {
		add = true
		already, _ = strconv.ParseInt(os.Args[2], 10, 8)
	}

	payload := Payload{}
	key := [32]byte{}
	sssKey := [32]byte{}
	paperKey := [32]byte{}
	var polynomial [][]byte
	if !recreate && !add {
		_, err = io.ReadFull(rand.Reader, sssKey[:])
		if err != nil {
			fmt.Printf("while creating sss key: %v\n", err)
			os.Exit(1)
		}
		_, err = io.ReadFull(rand.Reader, paperKey[:])
		if err != nil {
			fmt.Printf("while creating paperkey: %v\n", err)
			os.Exit(1)
		}
		_, err = io.ReadFull(rand.Reader, key[:])
		if err != nil {
			fmt.Printf("while creating key: %v\n", err)
			os.Exit(1)
		}
	} else {
		paperKeyWords, err := bufio.NewReader(os.Stdin).ReadString('\n')
		if err != nil {
			fmt.Printf("while reading paper key: %v\n", err)
			os.Exit(1)
		}
		pK, err := fromWords(paperKeyWords, wordlistRev)
		if err != nil {
			fmt.Printf("while decoding paper key: %v\n", err)
			os.Exit(1)
		}
		payloadFile, err := os.Open("payload")
		if err != nil {
			fmt.Printf("while opening payload: %v\n", err)
			os.Exit(1)
		}
		defer payloadFile.Close()

		payloadB64, err := io.ReadAll(payloadFile)
		if err != nil {
			fmt.Printf("while reading payload: %v\n", err)
			os.Exit(1)
		}

		payloadBytes, err := base64.StdEncoding.DecodeString(string(payloadB64))
		if err != nil {
			fmt.Printf("while de-base64-ing payload: %v\n", err)
			os.Exit(1)
		}

		payload, err = unmarshal(payloadBytes)
		if err != nil {
			fmt.Printf("while unmarshaling payload: %v\n", err)
			os.Exit(1)
		}

		k, err := decrypt(payload.Keys[1], pK)
		if err != nil {
			fmt.Printf("while decrypting paperKeyEnc: %v\n", err)
			os.Exit(1)
		}
		for i, pEnc := range payload.Polynomial {
			p, err := decrypt(pEnc, pK)
			if err != nil {
				fmt.Printf("while decrypting polynomial %d: %v\n", i, err)
				os.Exit(1)
			}
			polynomial = append(polynomial, p)
		}
		copy(key[:], k)
		copy(paperKey[:], pK)
	}

	passesExportFile, err := os.Open("secrets")
	if err != nil {
		fmt.Printf("while opening secrets: %v\n", err)
		os.Exit(1)
	}
	defer passesExportFile.Close()

	passes := []string{}
	s := bufio.NewScanner(passesExportFile)
	for s.Scan() {
		fields, err := shlex.Split(s.Text())
		if err != nil {
			fmt.Printf("while splitting line ‘%s’: %v", s.Text(), err)
			os.Exit(1)
		}
		mappedFields := make([]string, len(fields))
		for i, field := range fields {
			f := strings.SplitN(field, "=", 2)
			mappedFields[i] = strings.Join(f, ": ")
		}
		passes = append(passes, strings.Join(mappedFields, "\n"))
	}

	will, err := os.Open("will.pdf")
	if err != nil {
		fmt.Printf("while opening will: %v\n", err)
		os.Exit(1)
	}
	defer will.Close()

	willBytes, err := io.ReadAll(will)
	if err != nil {
		fmt.Printf("while reading will: %v\n", err)
		os.Exit(1)
	}

	payload.Secrets, err = encrypt([]byte(strings.Join(passes, "\n\n===\n\n")), key[:])
	payload.Will, err = encrypt(willBytes, key[:])

	if add {
		x, share, err := sss.Add(byte(already), 3, polynomial, sssKey[:])
		if err != nil {
			fmt.Printf("while spliting: %v\n", err)
			os.Exit(1)
		}
		share = append(share, x)
		fmt.Println(x, toWords(share, wordlist))
	} else if !recreate {
		sssKeyEnc, err := encrypt(key[:], sssKey[:])
		if err != nil {
			fmt.Printf("while encrypting sssKey: %v\n", err)
			os.Exit(1)
		}
		paperKeyEnc, err := encrypt(key[:], paperKey[:])
		if err != nil {
			fmt.Printf("while encrypting paperKey: %v\n", err)
			os.Exit(1)
		}
		payload.Keys = [][]byte{}
		payload.Keys = append(payload.Keys, sssKeyEnc)
		payload.Keys = append(payload.Keys, paperKeyEnc)

		shares, polynomial, err := sss.Split(5, 3, sssKey[:])
		if err != nil {
			fmt.Printf("while spliting: %v\n", err)
			os.Exit(1)
		}
		for i, p := range polynomial {
			pEnc, err := encrypt(p, paperKey[:])
			if err != nil {
				fmt.Printf("while encrypting polynomial %d: %v\n", i, err)
				os.Exit(1)
			}
			payload.Polynomial = append(payload.Polynomial, pEnc)
		}
		for x, share := range shares {
			share = append(share, x)
			fmt.Println(x, toWords(share, wordlist))
		}
		fmt.Println("")
		fmt.Println(toWords(paperKey[:], wordlist))
	}

	payloadBytes, err := bare.Marshal(&payload)
	if err != nil {
		fmt.Printf("while marshaling: %v\n", err)
		os.Exit(1)
	}
	payloadB64 := base64.StdEncoding.EncodeToString(payloadBytes)
	os.WriteFile("payload", []byte(payloadB64), 0o644)
}
