module apiote.xyz/p/joeblack

go 1.21

require (
	git.sr.ht/~sircmpwn/go-bare v0.0.0-20210406120253-ab86bc2846d9
	github.com/codahale/sss v0.0.0-20160501174526-0cb9f6d3f7f1
	github.com/google/shlex v0.0.0-20191202100458-e7afc7fbc510
)
