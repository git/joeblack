# git.sr.ht/~sircmpwn/go-bare v0.0.0-20210406120253-ab86bc2846d9
## explicit; go 1.14
git.sr.ht/~sircmpwn/go-bare
# github.com/codahale/sss v0.0.0-20160501174526-0cb9f6d3f7f1
## explicit
github.com/codahale/sss
# github.com/google/shlex v0.0.0-20191202100458-e7afc7fbc510
## explicit; go 1.13
github.com/google/shlex
